# Lenovo ThinkPad T540P 20BE00087GE OpenCore 0.62

## Prerequisites

### Hardware

|       Component       |                           Hardware                           | OpenCore relavant information |
| :-------------------: | :----------------------------------------------------------: | :---------------------------: |
|          CPU          | [Intel Core i7-4700MQ](https://ark.intel.com/content/www/us/en/ark/products/75117/intel-core-i7-4700mq-processor-6m-cache-up-to-3-40-ghz.html) |    **Code Name**: Haswell     |
|          GPU          | [NVIDIA GeForce GT 730M](https://www.geforce.com/hardware/notebook-gpus/geforce-gt-730m/specifications) |       Will be disabled        |
| Wireless Network Card | [Intel Dual Band Wireless-AC 7260](https://ark.intel.com/content/www/us/en/ark/products/75439/intel-dual-band-wireless-ac-7260.html) |                               |
|   Ethernet chipset    | [Intel Ethernet Connection I217-LM](https://ark.intel.com/content/www/us/en/ark/products/60019/intel-ethernet-connection-i217-lm.html) |                               |
|    Storage device     | [Samsung SSD 850 PRO 256GB](https://www.samsung.com/us/computing/memory-storage/solid-state-drives/ssd-850-pro-2-5-sata-iii-256gb-mz-7ke256bw/) |                               |

**See also**: [Specs](https://www.lenovo.com/de/de/laptops/thinkpad/t-series/T540p/p/22TP2TT540P)

## Getting started with OpenCore

Klick [here](https://dortania.github.io/OpenCore-Install-Guide/) for the [OpenCore Install Guide](https://dortania.github.io/OpenCore-Install-Guide/).



## Gathering files



### Firmware Drivers

> - **Location Note**: These files **must** be placed under `EFI/OC/Drivers/`

#### Universal

- [HfsPlus.efi](https://github.com/acidanthera/OcBinaryData/blob/master/Drivers/HfsPlus.efi)[ ](https://github.com/acidanthera/OcBinaryData/blob/master/Drivers/HfsPlus.efi)
- [OpenRuntime.efi](https://github.com/acidanthera/OpenCorePkg/releases)[ ](https://github.com/acidanthera/OpenCorePkg/releases)



### Kext

> **Location Note**: These files **must** be placed under `EFI/OC/Kexts/`

#### Must haves

- [Lilu](https://github.com/acidanthera/Lilu/releases)[ ](https://github.com/acidanthera/Lilu/releases)
  - A kext to patch many processes, required for AppleALC,  WhateverGreen, VirtualSMC and many other kexts. Without Lilu, they will  not work.
  - Note that Lilu and plugins requires OS X 10.8 or newer to function
- [VirtualSMC](https://github.com/acidanthera/VirtualSMC/releases)[ ](https://github.com/acidanthera/VirtualSMC/releases)/FakeSMC
  - Emulates the SMC chip found on real macs, without this macOS will not boot
  - Alternative is FakeSMC which can have better or worse support, most commonly used on legacy hardware.

---

**The following are inside the VirtualSMC package** (***optional***):

- SMCProcessor.kext
  - Used for monitoring CPU temperature
- SMCSuperIO.kext
  - Used for monitoring fan speed
- SMCBatteryManager.kext
  - Used for measuring battery readouts on laptops
  - Do not use until battery has been properly patched, can cause issues  otherwise. So for initial setup, please omit this kext. After install  you can follow this page for setup: [Fixing Battery Read-outs](https://dortania.github.io/OpenCore-Post-Install/laptop-specific/battery.html)[ ](https://dortania.github.io/OpenCore-Post-Install/laptop-specific/battery.html)

---

#### Graphics

- [WhateverGreen](https://github.com/acidanthera/WhateverGreen/releases)[ ](https://github.com/acidanthera/WhateverGreen/releases)

#### Audio

- [AppleALC](https://github.com/acidanthera/AppleALC/releases)[ ](https://github.com/acidanthera/AppleALC/releases)

#### Ethernet

- [IntelMausi](https://github.com/acidanthera/IntelMausi/releases)[ ](https://github.com/acidanthera/IntelMausi/releases)

#### USB

- [USBInjectAll](https://bitbucket.org/RehabMan/os-x-usb-inject-all/downloads/)[ ](https://bitbucket.org/RehabMan/os-x-usb-inject-all/downloads/)

#### Extras

- [CtlnaAHCIPort](https://github.com/dortania/OpenCore-Install-Guide/blob/master/extra-files/CtlnaAHCIPort.kext.zip)[ ](https://github.com/dortania/OpenCore-Install-Guide/blob/master/extra-files/CtlnaAHCIPort.kext.zip)

- [VoodooRMI](https://github.com/VoodooSMBus/VoodooRMI/releases/)[ ](https://github.com/VoodooSMBus/VoodooRMI/releases/)

  For systems with Synaptics' SMBus-based devices, mainly for trackpads and trackpoints.

---

For debugging later on:

- [NoTouchID](https://github.com/al3xtjames/NoTouchID/releases)[ ](https://github.com/al3xtjames/NoTouchID/releases)

  Recommended for MacBook SMBIOS that include a TouchID sensor to fix auth issues,  generally 2016 and newer SMBIOS will require this

---

### SSDTs

#### Needed SSDTs for Haswell laptops:

|           Component            |                             SSDT                             |
| :----------------------------: | :----------------------------------------------------------: |
|              CPU               | [SSDT-PLUG](https://dortania.github.io/Getting-Started-With-ACPI/Universal/plug.html)[ ](https://dortania.github.io/Getting-Started-With-ACPI/Universal/plug.html) |
|               EC               | [SSDT-EC](https://dortania.github.io/Getting-Started-With-ACPI/Universal/ec-fix.html)[ ](https://dortania.github.io/Getting-Started-With-ACPI/Universal/ec-fix.html) |
|           Backlight            | [SSDT-PNLF](https://dortania.github.io/Getting-Started-With-ACPI/Laptops/backlight.html)[ ](https://dortania.github.io/Getting-Started-With-ACPI/Laptops/backlight.html) |
| I2C Trackpad (do i need this?) | [SSDT-GPI0](https://dortania.github.io/Getting-Started-With-ACPI/Laptops/trackpad.html)[ ](https://dortania.github.io/Getting-Started-With-ACPI/Laptops/trackpad.html) |
|              AWAC              |                             N/A                              |
|              USB               |                             N/A                              |
|              IRQ               | [IRQ SSDT](https://dortania.github.io/Getting-Started-With-ACPI/Universal/irq.html)[ ](https://dortania.github.io/Getting-Started-With-ACPI/Universal/irq.html) |



## Getting started with ACPI

Location: `EFI/OC/ACPI`

#### Pre-Built SSDTs

###### Intel Laptop SSDTS

**Laptop Haswell and Broadwill**:

|                        SSDTs required                        |
| :----------------------------------------------------------: |
| [SSDT-PLUG-DRTNIA](https://github.com/dortania/Getting-Started-With-ACPI/blob/master/extra-files/compiled/SSDT-PLUG-DRTNIA.aml)[ ](https://github.com/dortania/Getting-Started-With-ACPI/blob/master/extra-files/compiled/SSDT-PLUG-DRTNIA.aml) |
| [SSDT-EC-USBX-LAPTOP](https://github.com/dortania/Getting-Started-With-ACPI/blob/master/extra-files/compiled/SSDT-EC-USBX-LAPTOP.aml)[ ](https://github.com/dortania/Getting-Started-With-ACPI/blob/master/extra-files/compiled/SSDT-EC-USBX-LAPTOP.aml) |
| [SSDT-PNLF](https://github.com/dortania/Getting-Started-With-ACPI/blob/master/extra-files/compiled/SSDT-PNLF.aml)[ ](https://github.com/dortania/Getting-Started-With-ACPI/blob/master/extra-files/compiled/SSDT-PNLF.aml) |
| [SSDT-XOSI](https://github.com/dortania/Getting-Started-With-ACPI/blob/master/extra-files/compiled/SSDT-XOSI.aml)[ ](https://github.com/dortania/Getting-Started-With-ACPI/blob/master/extra-files/compiled/SSDT-XOSI.aml) |



## config.plist Setup

Copy `Docs/Sample.plist` from [OpenCorePkg](https://github.com/acidanthera/OpenCorePkg/releases)[ ](https://github.com/acidanthera/OpenCorePkg/releases) and move it to your USB's EFI partition under `EFI/OC/` and rename it to `config.plist`



### Adding SSDTs, Kext and Firmware Drivers

Stopped at [DeviceProperties](https://dortania.github.io/OpenCore-Install-Guide/config-laptop.plist/haswell.html#deviceproperties)



## Configs

For debugging later on: [Kernel Quirks](https://dortania.github.io/OpenCore-Install-Guide/config-laptop.plist/haswell.html#quirks-3)

### [Intel BIOS settings](https://dortania.github.io/OpenCore-Install-Guide/config-laptop.plist/haswell.html#intel-bios-settings)

